#ifndef __MEMORY__
#define __MEMORY__

#include "buffer.h"

typedef void __mem_begin;


__mem_begin* init_block(file_buffer* buffer);
__mem_begin* open_block(file_buffer* buffer);

size_t alloc_block(__mem_begin*, size_t size, file_buffer*);
void free_block(__mem_begin*, size_t tofree, file_buffer*);

size_t get_unallocated_size(__mem_begin*);
size_t get_allocated_size(__mem_begin*);

void set_filesystem_main_header(__mem_begin*, size_t offset, file_buffer*);

size_t get_filesystem_main_header(__mem_begin*);

#endif
