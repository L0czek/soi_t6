#ifndef __FILESYSTEM__
#define __FILESYSTEM__

#include "buffer.h"

typedef void file_system;
typedef void file;

file_system* init_fs(file_buffer*);
file_system* open_fs(file_buffer*);
void close_fs(file_system* fs);

file* open_file_fs(file_system*, char*);
void close_file_fs(file*);

void list_dir_fs(file_system*, char*);

void mkdir_fs(file_system*, char*);
file* mkfile_fs(file_system*, char*);

size_t alloc_file_fs(file_system*, file*, size_t);
size_t realloc_file_fs(file_system*, file*, size_t);
void free_file_fs(file_system* fs, file*);

void rmdir_fs(file_system*, char*);
void rmfile_fs(file_system*, char*);

void link_fs(file_system*, char*, char*);
void rmlink_fs(file_system*, char*);

void copy_file_from_host(file_system*, char*, char*);
void copy_file_to_host(file_system*, char*, char*);

void print_stats(file_system* fs);
#endif
