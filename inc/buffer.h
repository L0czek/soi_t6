#ifndef __BUFFER__
#define __BUFFER__

#include <stdlib.h>

typedef void file_buffer;

file_buffer* open_file(char* name);
void file_buffer_dtor(file_buffer*);

size_t read_from_buffer(file_buffer*, size_t offset, size_t size, void* dest);
size_t write_to_buffer(file_buffer*, size_t offset, size_t size, void* src);

size_t get_file_size(file_buffer* this);

#endif
