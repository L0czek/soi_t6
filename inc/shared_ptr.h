#ifndef __SHARED_PTR__
#define __SHARED_PTR__

#include <stdlib.h>

typedef void shared_ptr;

#define deref(ptr, type) ((type*)deref_shared(ptr))
#define make(ptr, dtor) make_shared((void*)ptr, (void(*)(void*))dtor)


shared_ptr* make_shared(void* object, void (*dtor)(void*));
void free_shared(shared_ptr* ptr);
shared_ptr* copy_shared(shared_ptr* ptr);
void* deref_shared(shared_ptr* ptr);

void enter(size_t n, ...);
void leave(size_t n, ...);

#endif
