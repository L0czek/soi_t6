#include <stdlib.h>
#include <stdio.h>

typedef struct {
    FILE* file;
    size_t current_seek;
} file_buffer;

size_t get_file_size(file_buffer* this) {
    size_t cur_seek = ftell(this->file);
    fseek(this->file, 0, SEEK_END);
    size_t size = ftell(this->file);
    fseek(this->file, cur_seek, SEEK_SET);
    return size;
}
file_buffer* open_file(char* name) {
    FILE* file = fopen(name, "r+");
    if(!file) {
        return NULL;
    }
    file_buffer* ptr = (file_buffer*)malloc(sizeof(file_buffer));
    if(!ptr) {
        return NULL;
    }
    ptr->file = file;
    ptr->current_seek = 0;
    return ptr;
}

void file_buffer_dtor(file_buffer* ptr) {
    if(ptr){
        fclose(ptr->file);
        free(ptr);
    }
}

size_t read_from_buffer(file_buffer* this, size_t offset, size_t size, void* dest) {
    if(!this) {
        return 0;
    }
    if(this->current_seek != offset) {
        fseek(this->file, offset, SEEK_SET);
    }
    size_t bytes_read = fread(dest, 1, size, this->file);
    this->current_seek += bytes_read;
    return bytes_read;
}

size_t write_to_buffer(file_buffer* this, size_t offset, size_t size, void* src) {
    if(!this) {
        return 0;
    }
    if(this->current_seek != offset) {
        fseek(this->file, offset, SEEK_SET);
    }
    size_t bytes_written = fwrite(src, 1, size, this->file);
    this->current_seek += bytes_written;
    return bytes_written;
}
