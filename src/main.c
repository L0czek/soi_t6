#include <stdio.h>

#include "shared_ptr.h"
#include "buffer.h"
#include "mem.h"
#include "fs.h"
#include <string.h>

void exec(file_system* fs, int argc, char* argv[]);

void print_menu(void) {
    puts(
            "Usage: ./fs <disk option> -- <disk operation>\n"
            "Disk options:\n"
            "\t --open <disk name>\n"
            "\t --alloc <disk name> <disk size>\n"
            "Disk operations:\n"
            "\t ls <path>\n"
            "\t mkdir <path>\n"
            "\t mkfile <path>\n"
            "\t rmfile <path>\n"
            "\t rmdir <path>\n"
            "\t h2d <host_path> <disk_path>\n"
            "\t d2k <disk_path> <host_path>\n"
            "\t ln <source> <destination>\n"
            "\t stats\n"
        );
}

int main(int argc, char * argv[]) {
    if(argc >= 2 && strcmp(argv[1], "--open") == 0) {
        file_buffer* buffer = open_file(argv[2]);
        if(buffer == NULL) {
            puts("cannot open disk.");
            return -1;
        }
        file_system* fs = open_fs(buffer);
        if(argc > 4 && strcmp(argv[3], "--") == 0) {
            exec(fs, argc - 4, &argv[4]);
        }    
        close_fs(fs);
        file_buffer_dtor(buffer);
    } else if(argc >= 2 && strcmp(argv[1], "--alloc") == 0) {
        char* name = argv[2];
        int size = atoi(argv[3]);
        FILE* f = fopen(name, "w");
        if(f != NULL) {
            char* tmp = calloc(1, size);
            fwrite(tmp, 1, size, f);
            fclose(f);
            file_buffer* buffer = open_file(name);
            file_system* fs = init_fs(buffer);
            if(argc >= 5 && strcmp(argv[4], "--") == 0) {
                exec(fs, argc - 5, &argv[5]);
            }
            close_fs(fs);
            file_buffer_dtor(buffer);
        }
    } else {
        print_menu();
    }

}

void exec(file_system* fs, int argc, char* argv[]) {
    if(argc == 1 && strcmp(argv[0], "stats") == 0) {
        print_stats(fs);
    }else if(argc > 1) {
        if(strcmp(argv[0], "ls") == 0) {
            list_dir_fs(fs, argv[1]);
        } else if(strcmp(argv[0], "mkdir") == 0) {
            mkdir_fs(fs, argv[1]);
        } else if(strcmp(argv[0], "mkfile") == 0) {
            mkfile_fs(fs, argv[1]);
        } else if(strcmp(argv[0], "rmdir") == 0) {
            rmdir_fs(fs, argv[1]);
        } else if(strcmp(argv[0], "rmfile") == 0) {
            rmfile_fs(fs, argv[1]);
        } else if(strcmp(argv[0], "rmlink") == 0) {
            rmlink_fs(fs, argv[1]);
        }else if(argc > 2) {
            if(strcmp(argv[0], "h2d") == 0) {
                copy_file_from_host(fs, argv[1], argv[2]);
            } else if(strcmp(argv[0], "d2h") == 0) {
                copy_file_to_host(fs, argv[2], argv[1]);
            } else if(strcmp(argv[0], "ln") == 0) {
                link_fs(fs, argv[2], argv[1]);
            }    
        }else {
            print_menu();
        }
    } else{
        print_menu();
    }
}
