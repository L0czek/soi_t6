#include <stdlib.h>
#include <stdbool.h>

#include "buffer.h"
#include <string.h>

struct __mem_chunk {
    size_t next;
    size_t prev;
    size_t size;
    bool used;    
};

typedef struct __mem_chunk __mem_chunk;

typedef struct {
    size_t total_size;
    size_t allocated;
    size_t head;
    size_t fs_begin;
}__mem_begin;

size_t get_unallocated_size(__mem_begin* this) {
    return (size_t)(this->total_size - this->allocated);
}

size_t get_allocated_size(__mem_begin* this) {
    return this->allocated;
}
__mem_begin* init_block(file_buffer* buffer) {
    size_t file_size = get_file_size(buffer);
    size_t allocated = sizeof(__mem_chunk) + sizeof(__mem_begin);
    size_t unallocated = file_size - allocated;


    __mem_begin heap;
    memset(&heap, 0, sizeof(heap));
    heap.total_size = file_size;
    heap.allocated = allocated;
    heap.head = sizeof(__mem_begin);
    

    __mem_chunk first;
    memset(&first, 0, sizeof(first));
    first.next = 0;
    first.prev = 0;
    first.size = unallocated;
    first.used = false;
    

    write_to_buffer(buffer, 0, sizeof(__mem_begin), &heap);
    write_to_buffer(buffer, sizeof(__mem_begin), sizeof(__mem_chunk), &first);

    __mem_begin* ptr = (__mem_begin*)malloc(sizeof(__mem_begin));
    *ptr = heap;
    return ptr;
}

__mem_begin* open_block(file_buffer* buffer) {
    __mem_begin* ptr = (__mem_begin*)malloc(sizeof(__mem_begin));
    read_from_buffer(buffer, 0, sizeof(__mem_begin), ptr);
    return ptr;
}

size_t alloc_block_impl(size_t head, size_t size, file_buffer* buffer) {
    __mem_chunk cur;
    read_from_buffer(buffer, head, sizeof(cur), &cur);
    size_t cur_offset = head;
    while(true) {
        if(!cur.used) {
            if(cur.size > (sizeof(__mem_chunk) + size)) {
                size_t new_offset = cur_offset + sizeof(__mem_chunk) + size;
                size_t new_size = cur.size - sizeof(__mem_chunk) - size;

                __mem_chunk new;
                memset(&new, 0, sizeof(new));
                new.next = cur.next;
                new.prev = cur_offset;
                new.size = new_size;
                new.used = false;

                if(cur.next) {
                    __mem_chunk third;
                    memset(&third, 0, sizeof(third));
                    read_from_buffer(buffer, cur.next, sizeof(third), &third);
                    third.prev = new_offset;
                    write_to_buffer(buffer, cur.next, sizeof(third), &third);
                }
                
                cur.next = new_offset;
                cur.prev = cur.prev;
                cur.size = size;
                cur.used = true;
                

                write_to_buffer(buffer, cur_offset, sizeof(__mem_chunk), &cur);
                write_to_buffer(buffer, new_offset, sizeof(__mem_chunk), &new);

                return cur_offset + sizeof(__mem_chunk);
            }
        }
        size_t next_offset = cur.next;
        if(next_offset == 0) {
            return 0;
        }
        read_from_buffer(buffer, next_offset, sizeof(__mem_chunk), &cur);
        cur_offset = next_offset;
    }
}

size_t alloc_block(__mem_begin* this, size_t size, file_buffer* buffer ) {
    size_t csize = size;
    if(size % 8 != 0) {
        csize += 8 - (size % 8);
    }
    if(get_unallocated_size(this) < csize + sizeof(__mem_chunk)) {
        return 0;
    }
    size_t offset = alloc_block_impl(this->head, size, buffer);
    if(offset != 0) {
        this->allocated += csize + sizeof(__mem_chunk);
        write_to_buffer(buffer, 0, sizeof(__mem_begin), this);
    }
    return offset;
}

bool try_consolidate(__mem_chunk* first, __mem_chunk* second, file_buffer* buffer) {
    if(first->used || second->used) {
        return false;
    }
    first->next = second->next;
    first->size += sizeof(__mem_chunk) + second->size;
    if(second->next) {
        __mem_chunk third;
        read_from_buffer(buffer, second->next, sizeof(__mem_chunk), &third);
        third.prev = second->prev;
        write_to_buffer(buffer, second->next, sizeof(__mem_chunk), &third);
    }
    return true;
}

void free_block(__mem_begin* this, size_t offset, file_buffer* buffer) {
    __mem_chunk chunk;
    size_t chunk_offset = offset - sizeof(__mem_chunk);
    if(read_from_buffer(buffer, chunk_offset, sizeof(__mem_chunk), &chunk) != sizeof(__mem_chunk)) {
        abort();
    }
    chunk.used = false;
    this->allocated -= chunk.size;
    this->allocated -= sizeof(__mem_chunk);
    write_to_buffer(buffer, 0, sizeof(__mem_begin), this);
    if(chunk.next != 0) {
        __mem_chunk next;
        size_t next_offset = chunk.next;
        read_from_buffer(buffer, next_offset, sizeof(__mem_chunk), &next);
        if(try_consolidate(&chunk, &next, buffer)) {
            memset(&next, 0, sizeof(next));
            write_to_buffer(buffer, next_offset, sizeof(next), &next);
        }
    }
    if(chunk.prev != 0) {
        __mem_chunk prev;
        size_t prev_offset = chunk.prev;
        read_from_buffer(buffer, prev_offset, sizeof(__mem_chunk), &prev);
        if(try_consolidate(&prev, &chunk, buffer)) {
            memset(&chunk, 0, sizeof(chunk));
            write_to_buffer(buffer, prev_offset, sizeof(prev), &prev);
        }
    }
    write_to_buffer(buffer, chunk_offset, sizeof(chunk), &chunk);
}

void set_filesystem_main_header(__mem_begin* this, size_t offset, file_buffer* buffer) {
    this->fs_begin = offset;
    write_to_buffer(buffer, 0, sizeof(__mem_begin), this);
}

size_t get_filesystem_main_header(__mem_begin* heap) {
    return heap->fs_begin;
}
