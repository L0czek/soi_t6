#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "buffer.h"
#include "mem.h"
#include <stdint.h>

typedef struct {
    char magic[4]; // FSMH
    uint64_t root_directory;

} __filesystem_header;


typedef struct {
    char magic[4]; // FSFH
    uint64_t next_node_in_dir;
    uint64_t name_size;
    uint64_t name_data_offset;
    uint64_t file_size;
    uint64_t file_data_offset;
} __file_header;


typedef struct {
    char magic[4]; // FSDH
    uint64_t next_node_in_dir;
    uint64_t name_size;
    uint64_t name_data_offset;
    uint64_t first_node_in_dir;
} __directory_header;

typedef struct {
    char magic[4]; // FSLH
    uint64_t next_node_in_dir;
    uint64_t name_size;
    uint64_t name_data_offset;
    uint64_t link_to;
} __link_header;

typedef struct {
    file_buffer* buffer;
    __mem_begin* heap;
} file_system;

typedef struct file {
    size_t header_offset;
    size_t data_offset;
    size_t data_size;
} file;

enum HeaderType {
    FSMH,
    FSFH,
    FSDH,
    FSLH
};

void init_filesystem_header(__filesystem_header* ptr) {
    memset(ptr, 0, sizeof(__filesystem_header));
    strncpy(&ptr->magic[0], "FSMH", 4);
}

void init_file_header(__file_header* ptr) {
    memset(ptr, 0, sizeof(__file_header));
    strncpy(&ptr->magic[0], "FSFH", 4);
}

void init_directory_header(__directory_header* ptr) {
    memset(ptr, 0, sizeof(__directory_header));
    strncpy(&ptr->magic[0], "FSDH", 4);
}

void init_link_header(__link_header* ptr) {
    memset(ptr, 0, sizeof(__link_header));
    strncpy(&ptr->magic[0], "FSLH", 4);
}
file* open_file_fs(file_system*, char*);
void close_file_fs(file*);
file_system* init_fs(file_buffer* buffer) {
    if(buffer == NULL) {
        return NULL;
    }
    void* heap = init_block(buffer);
    if(!heap) {
        return NULL;
    }
    file_system* ptr = (file_system*)malloc(sizeof(file_system));
    if(!ptr) {
        return NULL;
    }
    ptr->buffer = buffer;
    ptr->heap = heap;

    __filesystem_header main_header;
    init_filesystem_header(&main_header);

    __directory_header root_directory;
    init_directory_header(&root_directory);

    size_t main_header_offset = alloc_block(heap, sizeof(__filesystem_header), buffer);
    size_t root_directory_offset = alloc_block(heap, sizeof(__directory_header), buffer);
    main_header.root_directory = root_directory_offset;
    
    size_t name_offset = alloc_block(heap, 2, buffer);
    write_to_buffer(buffer, name_offset, 2, "/");
    root_directory.name_data_offset = name_offset;
    root_directory.name_size = 1;

    write_to_buffer(buffer, main_header_offset, sizeof(__filesystem_header), &main_header);
    write_to_buffer(buffer, root_directory_offset, sizeof(__directory_header), &root_directory);

    set_filesystem_main_header(heap, main_header_offset, buffer);
    
    return ptr;
}

file_system* open_fs(file_buffer* buffer) {
    __mem_begin* heap = open_block(buffer);
    if(heap == NULL) {
        return NULL;
    }
    file_system* fs = (file_system*)malloc(sizeof(file_system));
    if(fs == NULL) {
        return NULL;
    }
    fs->buffer = buffer;
    fs->heap = heap;
    return fs;
}

void close_fs(file_system* fs) {
    if(!fs) {
        return;
    }
    free(fs->heap);
    free(fs);
}

size_t go_to_dir(file_system* fs, char* path) {
    if(path == NULL) {
        return 0;
    }
    char* copy = strdup(path);

    size_t main_header_offset = get_filesystem_main_header(fs->heap);
    __filesystem_header fs_header;
    read_from_buffer(fs->buffer, main_header_offset, sizeof(__filesystem_header), &fs_header);
    size_t root_dir_offset = fs_header.root_directory;
    __directory_header dir_header;
    read_from_buffer(fs->buffer, root_dir_offset, sizeof(__directory_header), &dir_header);
    char* token = strtok(copy, "/");
    if(token == NULL) {
        free(copy);
        return root_dir_offset;
    }

    char buffer[0x100];
    if(!dir_header.first_node_in_dir) {
        free(copy);
        return 0;
    }
    read_from_buffer(fs->buffer, dir_header.first_node_in_dir, sizeof(buffer), buffer);
    char* name = NULL;
    size_t current_offset = dir_header.first_node_in_dir;
    size_t name_size = 0;
    size_t name_offset = 0;
    size_t next_offset = 0;
    char* next_token = NULL;
    __file_header* ptr_fh = (__file_header*)buffer;
    __directory_header* ptr_dh = (__directory_header*)buffer;
    __link_header* ptr_lh = (__link_header*)buffer;
    enum HeaderType type;
    while(token != NULL) {
            if(memcmp(buffer, "FSDH", 4) == 0) {
                type = FSDH;
            } else if(memcmp(buffer, "FSFH", 4) == 0) {
                type = FSFH;
            } else if(memcmp(buffer, "FSLH", 4) == 0) {
                type = FSLH;
            }
            switch(type) {
                case FSFH: name_size = ptr_fh->name_size; name_offset = ptr_fh->name_data_offset; break;
                case FSDH: name_size = ptr_dh->name_size; name_offset = ptr_dh->name_data_offset; break;
                case FSLH: name_size = ptr_lh->name_size; name_offset = ptr_lh->name_data_offset; break;
            }
            name = realloc(name, name_size+1);
            read_from_buffer(fs->buffer, name_offset, name_size, name);
            name[name_size] = 0;
            if(strcmp(token, name) == 0) {
                switch(type) {
                    case FSDH: next_offset = ptr_dh->first_node_in_dir; next_token = strtok(NULL, "/"); break;
                    case FSFH:
                               next_token = strtok(NULL, "/");
                               free(copy);
                               free(name);
                               if(next_token != NULL) {
                                    printf("error `%s` is not a direcotry.\n", token);
                    //                free(copy);
                      //              free(name);
                                    return 0;
                               }
                               return current_offset;
                               break;
                    case FSLH:
                               do {
                                    current_offset = ptr_lh->link_to;
                                    read_from_buffer(fs->buffer, current_offset, sizeof(buffer), buffer);
                               }while(memcmp(buffer, "FSLH", 4) == 0);
                               if(memcmp(buffer, "FSFH", 4) == 0) {
                                   next_token = strtok(NULL, "/");
                                   if(next_token != NULL) {
                                       printf("error `%s` is not a direcotry.\n", token);
                                       return 0;
                                   }
                                   
                                       free(name);
                                       free(copy);
                                   return current_offset;
                               } else if(memcmp(buffer, "FSDH", 4) == 0) {
                                   next_offset = ptr_dh->first_node_in_dir;
                                   next_token = strtok(NULL, "/");
                               }

                }
                if(next_token != NULL) {
                    read_from_buffer(fs->buffer, next_offset, sizeof(buffer), buffer);
                    current_offset = next_offset;
                    token = next_token;
                } else {
                    free(copy);
                    free(name);
                    return current_offset;
                }
            } else {
                current_offset = ptr_fh->next_node_in_dir;
                if(current_offset == 0) {
                    next_token = strtok(NULL, "/");
                    free(copy);
                    free(name);
                    if(next_token != NULL) {
                        printf("error no such file or directory.\n");
                        return 0;
                    } else {
                        return current_offset;
                    }
                }
                read_from_buffer(fs->buffer, current_offset, sizeof(buffer), buffer);
            }       
    }
    free(copy);
    free(name);
    if(token == NULL) {
        return current_offset;
    } else {
        return 0;
    }
}

void print_info_about(file_system* fs, char* buffer) {
    __file_header* ptr = (__file_header*)buffer;
    __directory_header *hptr = (__directory_header*)buffer;
    __link_header* lptr = (__link_header*)buffer;
    size_t name_offset = ptr->name_data_offset;
    size_t name_size = ptr->name_size;
    char* name = (char*)malloc(name_size+1);
    read_from_buffer(fs->buffer, name_offset, name_size, name);
    name[name_size] = 0;
    if(memcmp(buffer, "FSFH", 4) == 0) {
        size_t file_size = ptr->file_size;
        printf("file: %s; size: %u\n", name, file_size);
    } else if(memcmp(buffer, "FSDH", 4) == 0) {
        printf("dir: %s\n", name);
    } else if(memcmp(buffer, "FSLH", 4) == 0) {
        size_t link_to = lptr->link_to;
        char buffer2[0x100];
        read_from_buffer(fs->buffer, link_to, sizeof(buffer2), buffer2);
        printf("symlink: %s; linked with ", name);
        print_info_about(fs, buffer2);
    }
    free(name);
}

void list_dir_fs(file_system* fs, char* path) {
    size_t dest = go_to_dir(fs, path);
    printf("%s:\n", path);
    if(dest == 0) {
        printf("empty\n");
        return;
    }
    char buffer[0x100];
    read_from_buffer(fs->buffer, dest, sizeof(buffer), buffer);
    if(memcmp(buffer, "FSFH", 4) == 0) {
        print_info_about(fs, buffer); 
    } else if(memcmp(buffer, "FSDH", 4) == 0) {
        __directory_header* ptr = (__directory_header*)buffer;
        size_t current_node = ptr->first_node_in_dir;
        char* name = NULL;
        while(current_node) {
            read_from_buffer(fs->buffer, current_node, sizeof(buffer), buffer);
            print_info_about(fs, buffer);
            current_node = ptr->next_node_in_dir;
        }
        free(name);
    }
}

typedef struct {
    char* path;
    char* name;
} fname;

void init_fname(fname* ptr, char* path) {
    size_t len = strlen(path);
    if(len == 0 || path[0] != '/') {
        ptr->name = NULL;
        ptr->path = NULL;
    }
    for(size_t i=0; i < len; ++i) {
        if(path[len-i-1] == '/') {
            ptr->path = malloc(len-i+1);
            ptr->name = malloc(i+1);
            memcpy(ptr->path, &path[0], len-i);
            memcpy(ptr->name, &path[len-i], i);
            ptr->path[len-i] = 0;
            ptr->name[i] = 0;
            break;
        }
    }
}

void clear_fname(fname* ptr) {
    free(ptr->path);
    free(ptr->name);
}

void mkdir_fs(file_system* fs, char* path) {
    fname name;
    init_fname(&name, path);
    size_t dest = go_to_dir(fs, name.path);
    if(dest == 0) {
        printf("`%s` no such file or direcotry.\n", path);
        clear_fname(&name);
        return;
    }
    char buffer[0x100];
    read_from_buffer(fs->buffer, dest, sizeof(buffer), buffer);
    if(memcmp(buffer, "FSDH", 4) != 0) {
        printf("`%s` is not a directory.\n", path);
        clear_fname(&name);
        return;
    }
    __directory_header* ptr = (__directory_header*)buffer;
    __directory_header header;
    init_directory_header(&header);
    size_t header_offset = alloc_block(fs->heap, sizeof(header), fs->buffer);
    size_t name_size = strlen(name.name);
    size_t name_offset = alloc_block(fs->heap, name_size, fs->buffer);
    write_to_buffer(fs->buffer, name_offset, name_size, name.name);
    header.name_size = name_size;
    header.name_data_offset = name_offset;
    header.next_node_in_dir = ptr->first_node_in_dir;
    ptr->first_node_in_dir = header_offset;
    write_to_buffer(fs->buffer, header_offset, sizeof(header), &header);
    write_to_buffer(fs->buffer, dest, sizeof(__directory_header), buffer);
    clear_fname(&name);
}

file* mkfile_fs(file_system* fs, char* path) {
    file* in_disk = open_file_fs(fs, path);
    if(in_disk != NULL) {
        close_file_fs(in_disk);
        return NULL;
    }
    fname name;
    init_fname(&name, path);
    size_t dest = go_to_dir(fs, name.path);
    if(dest == 0) {
        printf("`%s` no such file or direcotry.\n");
        clear_fname(&name);
        return NULL;
    }
    char buffer[0x100];
    read_from_buffer(fs->buffer, dest, sizeof(buffer), buffer);
    if(memcmp(buffer, "FSDH", 4) != 0) {
        printf("`%s` is not a directory.\n");
        clear_fname(&name);
        return NULL;
    }
    __directory_header* ptr = (__directory_header*)buffer;
    __file_header new_file;
    init_file_header(&new_file);
    size_t file_header_offset = alloc_block(fs->heap, sizeof(__file_header), fs->buffer);
    size_t name_size = strlen(name.name);
    size_t name_offset = alloc_block(fs->heap, name_size, fs->buffer);
    new_file.name_size = name_size;
    new_file.name_data_offset = name_offset;
    new_file.next_node_in_dir = ptr->first_node_in_dir;
    ptr->first_node_in_dir = file_header_offset;
    write_to_buffer(fs->buffer, name_offset, name_size, name.name);
    write_to_buffer(fs->buffer, file_header_offset, sizeof(__file_header), &new_file);
    write_to_buffer(fs->buffer, dest, sizeof(__directory_header), ptr);
    clear_fname(&name);
    file* ret = (file*)malloc(sizeof(file));
    if(!ret) {
        abort();
    }
    ret->header_offset = file_header_offset;
    ret->data_offset = 0;
    ret->data_size = 0;
    return ret;
}

void close_file_fs(file* ptr) {
    free(ptr);
}

file* open_file_fs(file_system* fs, char* path) {
    size_t dest = go_to_dir(fs, path);
    if(dest == 0) {
        return NULL;
    }
    char buffer[0x100];
    read_from_buffer(fs->buffer, dest, sizeof(buffer), buffer);
    if(memcmp(buffer, "FSFH", 4) != 0) {
        return NULL;
    }
    __file_header* ptr = (__file_header*)buffer;
    file* ret = (file*)malloc(sizeof(file));
    ret->header_offset = dest;
    ret->data_offset = ptr->file_data_offset;
    ret->data_size = ptr->file_size;
    return ret;
}

size_t alloc_file_fs(file_system* fs, file* f, size_t size) {
    __file_header header;
    read_from_buffer(fs->buffer, f->header_offset, sizeof(header), &header);
    if(header.file_data_offset) {
        return 0;
    }
    header.file_data_offset = alloc_block(fs->heap, size, fs->buffer);
    header.file_size = size;
    write_to_buffer(fs->buffer, f->header_offset, sizeof(header), &header);
    return header.file_data_offset;
}

size_t realloc_file_fs(file_system* fs, file* f, size_t size) {
    __file_header header;
    read_from_buffer(fs->buffer, f->header_offset, sizeof(header), &header);
    if(header.file_data_offset == 0) {
        header.file_data_offset = alloc_block(fs->heap, size, fs->buffer);
        header.file_size = size;
        write_to_buffer(fs->buffer, f->header_offset, sizeof(header), &header);
    } else {
        size_t new_buffer = alloc_block(fs->heap, size, fs->buffer);
        if(new_buffer == 0) {
            return 0;
        }
        size_t old_buffer = header.file_data_offset;
        size_t to_copy = header.file_size;
        char* buffer = (char*)malloc(to_copy);
        to_copy = to_copy <= size ? to_copy : size;
        read_from_buffer(fs->buffer, old_buffer, to_copy, buffer);
        write_to_buffer(fs->buffer, new_buffer, to_copy, buffer);
        free(buffer);
        free_block(fs->heap, old_buffer, fs->buffer);
        header.file_size = to_copy;
        header.file_data_offset = new_buffer;
        write_to_buffer(fs->buffer, f->header_offset, sizeof(__file_header), &header);
    }
    return header.file_data_offset;
}

void free_file_fs(file_system* fs, file* f) {
    __file_header header;
    read_from_buffer(fs->buffer, f->header_offset, sizeof(__file_header), &header);
    if(header.file_data_offset != 0) {
        free_block(fs->heap, header.file_data_offset, fs->buffer);
        header.file_data_offset = 0;
        write_to_buffer(fs->buffer, f->header_offset, sizeof(__file_header), &header);
    }
}

void rmdir_fs(file_system* fs, char* path) {
    fname name;
    init_fname(&name, path);
    size_t dest = go_to_dir(fs, name.path);
    if(dest == 0) {
        printf("Error `%s` no such file or directory.\n", path);
        clear_fname(&name);
        return;
    }
    char buffer[0x100];
    read_from_buffer(fs->buffer, dest, sizeof(buffer), buffer);
    __directory_header* ptr = (__directory_header*)buffer;
    size_t current_node = ptr->first_node_in_dir;
    if(current_node == 0){
         printf("Error `%s` no such file or directory.\n", path);
         clear_fname(&name);
         return;
    }
    char buffer2[0x100];
    read_from_buffer(fs->buffer, current_node, sizeof(buffer2), buffer2);
    __directory_header* hptr = (__directory_header*)buffer2;
    char* dname = NULL;
    size_t name_size = hptr->name_size;
    size_t name_offset = hptr->name_data_offset;
    dname = realloc(dname, name_size+1);
    read_from_buffer(fs->buffer, name_offset, name_size, dname);
    if(memcmp(dname, name.name, name_size) == 0) {
        if(memcmp(buffer2, "FSDH", 4) != 0) {
            printf("Error `%s` is not a directory.\n", path);
            clear_fname(&name);
            free(dname);
            return;
        }
        if(hptr->first_node_in_dir != 0) {
            printf("Error directory is not empty.\n");
            clear_fname(&name);
            free(dname);
            return;
        }
        ptr->first_node_in_dir = hptr->next_node_in_dir;
        free_block(fs->heap, hptr->name_data_offset, fs->buffer);
        free_block(fs->heap, current_node, fs->buffer);
        write_to_buffer(fs->buffer, dest, sizeof(__directory_header), ptr);
        free(dname);
        return;
    }
    size_t previous_node = current_node;
    current_node = hptr->next_node_in_dir;
    while(current_node) {
        memcpy(buffer, buffer2, sizeof(buffer));
        read_from_buffer(fs->buffer, current_node, sizeof(buffer2), buffer2);
        name_size = hptr->name_size;
        name_offset = hptr->name_data_offset;
        dname = realloc(dname, name_size+1);
        read_from_buffer(fs->buffer, name_offset, name_size, dname);
        dname[name_size] = 0;
        if(memcmp(dname, name.name, name_size) == 0) {
            if(memcmp(buffer2, "FSDH", 4) != 0) {
                printf("`%s` is not a directory.\n", path);
                clear_fname(&name);
                free(dname);
                return;
            }
            if(hptr->first_node_in_dir != 0) {
                printf("Error directory not empty\n");
                clear_fname(&name);
                free(dname);
                return;
            }
            ptr->next_node_in_dir = hptr->next_node_in_dir;
            free_block(fs->heap, hptr->name_data_offset, fs->buffer);
            free_block(fs->heap, current_node, fs->buffer);
            write_to_buffer(fs->buffer, previous_node, 16, buffer);
            clear_fname(&name);
            free(dname);
            return;
        }
        previous_node = current_node;
        current_node = hptr->next_node_in_dir;
    }
    printf("`%s` no such file in directory.\n", path);
    free(dname);
    clear_fname(&name);
}

void rmfile_fs(file_system* fs, char* path) {
    fname name;
    init_fname(&name, path);
    size_t dest = go_to_dir(fs, name.path);
    if(dest == 0) {
        printf("Error `%s` no such file or directory.\n", path);
        clear_fname(&name);
        return;
    }
    char buffer[0x100];
    read_from_buffer(fs->buffer, dest, sizeof(buffer), buffer);
    __directory_header* ptr = (__directory_header*)buffer;
    size_t current_node = ptr->first_node_in_dir;
    if(current_node == 0){
         printf("Error `%s` no such file or directory.\n", path);
         clear_fname(&name);
         return;
    }
    char buffer2[0x100];
    read_from_buffer(fs->buffer, current_node, sizeof(buffer2), buffer2);
    __file_header* fptr = (__file_header*)buffer2;
     char* dname = NULL;
    size_t name_size = fptr->name_size;
    size_t name_offset = fptr->name_data_offset;
    dname = realloc(dname, name_size+1);
    read_from_buffer(fs->buffer, name_offset, name_size, dname);
    if(memcmp(dname, name.name, name_size) == 0) {
        if(memcmp(buffer2, "FSFH", 4) != 0) {
            printf("Error `%s` is not a file\n", path);
            clear_fname(&name);
            free(dname);
            return;
        } 
        ptr->first_node_in_dir = fptr->next_node_in_dir;
        free_block(fs->heap, fptr->name_data_offset, fs->buffer);
        if(fptr->file_data_offset) {
            free_block(fs->heap, fptr->file_data_offset, fs->buffer);
        }
        free_block(fs->heap, current_node, fs->buffer);
        write_to_buffer(fs->buffer, dest, sizeof(__directory_header), ptr);
        free(dname);
        return;
    }
    size_t previous_node = current_node;
    current_node = fptr->next_node_in_dir;
    while(current_node) {
        memcpy(buffer, buffer2, sizeof(buffer));
        read_from_buffer(fs->buffer, current_node, sizeof(buffer2), buffer2);
        name_size = fptr->name_size;
        name_offset = fptr->name_data_offset;
        dname = realloc(dname, name_size+1);
        read_from_buffer(fs->buffer, name_offset, name_size, dname);
        dname[name_size] = 0;
        if(memcmp(dname, name.name, name_size) == 0) {
            if(memcmp(buffer2, "FSFH", 4) != 0) {
                printf("`%s` is not a file.\n", path);
                clear_fname(&name);
                free(dname);
                return;
            } 
            ptr->next_node_in_dir = fptr->next_node_in_dir;
            free_block(fs->heap, fptr->name_data_offset, fs->buffer);
            if(fptr->file_data_offset) {
                free_block(fs->heap, fptr->file_data_offset, fs->buffer);
            }
            free_block(fs->heap, current_node, fs->buffer);
            write_to_buffer(fs->buffer, previous_node, 16, buffer);
            clear_fname(&name);
            free(dname);
            return;
        }
        previous_node = current_node;
        current_node = fptr->next_node_in_dir;
    }
    printf("`%s` no such file in directory.\n", path);
    free(dname);
    clear_fname(&name);
}

void link_fs(file_system* fs, char* link_name, char* link_to) {
    size_t src = go_to_dir(fs, link_to);
    if(src == 0) {
        printf("Error `%s` no such file or directory.\n", link_to);
        return;
    }
    fname name;
    init_fname(&name, link_name);
    size_t dest = go_to_dir(fs, name.path);
    if(dest == 0) {
        printf("Error `%s` no such file or directory.\n", name.path);
        clear_fname(&name);
        return;
    }
    __directory_header dir_header;
    __link_header link_header;
    init_link_header(&link_header);
    link_header.link_to = src;
    read_from_buffer(fs->buffer, dest, sizeof(__directory_header), &dir_header);
    size_t name_size = strlen(name.name);
    size_t name_offset = alloc_block(fs->heap, name_size, fs->buffer);
    write_to_buffer(fs->buffer, name_offset, name_size, name.name);
    link_header.name_size = name_size;
    link_header.name_data_offset = name_offset;
    size_t header_offset = alloc_block(fs->heap, sizeof(__link_header), fs->buffer);
    link_header.next_node_in_dir = dir_header.first_node_in_dir;
    dir_header.first_node_in_dir = header_offset;
    write_to_buffer(fs->buffer, header_offset, sizeof(__link_header), &link_header);
    write_to_buffer(fs->buffer, dest, sizeof(__directory_header), &dir_header);    
    clear_fname(&name);
}

void rmlink_fs(file_system* fs, char* path) {
    fname name;
    init_fname(&name, path);
    size_t dest = go_to_dir(fs, name.path);
    if(dest == 0) {
        printf("Error `%s` no such file or directory.\n", path);
        clear_fname(&name);
        return;
    }
    char buffer[0x100];
    read_from_buffer(fs->buffer, dest, sizeof(buffer), buffer);
    __directory_header* ptr = (__directory_header*)buffer;
    size_t current_node = ptr->first_node_in_dir;
    if(current_node == 0){
         printf("Error `%s` no such file or directory.\n", path);
         clear_fname(&name);
         return;
    }
    char buffer2[0x100];
    read_from_buffer(fs->buffer, current_node, sizeof(buffer2), buffer2);
    __link_header* lptr = (__link_header*)buffer2;
     char* dname = NULL;
    size_t name_size = lptr->name_size;
    size_t name_offset = lptr->name_data_offset;
    dname = realloc(dname, name_size+1);
    read_from_buffer(fs->buffer, name_offset, name_size, dname);
    if(memcmp(dname, name.name, name_size) == 0) {
        if(memcmp(buffer2, "FSLH", 4) != 0) {
            printf("Error `%s` is not a symlink\n", path);
            clear_fname(&name);
            free(dname);
            return;
        } 
        ptr->first_node_in_dir = lptr->next_node_in_dir;
        free_block(fs->heap, lptr->name_data_offset, fs->buffer); 
        free_block(fs->heap, current_node, fs->buffer);
        write_to_buffer(fs->buffer, dest, sizeof(__directory_header), ptr);
        free(dname);
        clear_fname(&name);
        return;
    }
    size_t previous_node = current_node;
    current_node = lptr->next_node_in_dir;
    while(current_node) {
        memcpy(buffer, buffer2, sizeof(buffer));
        read_from_buffer(fs->buffer, current_node, sizeof(buffer2), buffer2);
        name_size = lptr->name_size;
        name_offset = lptr->name_data_offset;
        dname = realloc(dname, name_size+1);
        read_from_buffer(fs->buffer, name_offset, name_size, dname);
        dname[name_size] = 0;
        if(memcmp(dname, name.name, name_size) == 0) {
            if(memcmp(buffer2, "FSLH", 4) != 0) {
                printf("`%s` is not a symlink.\n", path);
                clear_fname(&name);
                free(dname);
                return;
            } 
            ptr->next_node_in_dir = lptr->next_node_in_dir;
            free_block(fs->heap, lptr->name_data_offset, fs->buffer);
            free_block(fs->heap, current_node, fs->buffer);
            write_to_buffer(fs->buffer, previous_node, 16, buffer);
            clear_fname(&name);
            free(dname);
            return;
        }
        previous_node = current_node;
        current_node = lptr->next_node_in_dir;
    }
    printf("`%s` no such file in directory.\n", path);
    free(dname);
    clear_fname(&name);

}

void copy_file_from_host(file_system* fs, char* host_path, char* path) {
    file* fd = mkfile_fs(fs, path);
    if(fd == NULL) {
        fd = open_file_fs(fs, path);
    }
    if(fd == NULL) {
        printf("Error cannot create file.\n");
        return;
    }
    FILE* f = fopen(host_path, "r");
    if(f == NULL) {
        printf("Error cannot open path on host.\n");
        rmfile_fs(fs, path);
        close_file_fs(fd);
        return;
    }
    fseek(f, 0, SEEK_END);
    size_t size = ftell(f);
    fseek(f, 0, SEEK_SET);
    size_t alloc = realloc_file_fs(fs, fd, size);
    if(alloc == 0) {
        printf("cannot allocate file.\n");
        fclose(f);
        close_file_fs(fd);
        rmfile_fs(fs, path);
        return;
    }
    char* content = malloc(size);
    fread(content, 1, size, f);
    write_to_buffer(fs->buffer, alloc, size, content);
    fclose(f);
    close_file_fs(fd);
}

void copy_file_to_host(file_system* fs, char* host_path, char* path) {
    file* fd = open_file_fs(fs, path);
    if(fd == NULL) {
        printf("Error `%s` no such file or directory.\n", path);
        return;
    }
    FILE* f = fopen(host_path, "w");
    if(f == NULL) {
        printf("cannot crate file on host.\n");
        close_file_fs(fd);
        return;
    }
    if(fd->data_offset == 0) {
        printf("error empty file.\n");
        close_file_fs(fd);
        fclose(f);
        return;
    }
    char* content = (char*)malloc(fd->data_size);
    read_from_buffer(fs->buffer, fd->data_offset, fd->data_size, content);
    fwrite(content, 1, fd->data_size, f);
    fclose(f);
    close_file_fs(fd);
}

void print_stats(file_system* fs) {
    printf(
            "Disk info:\n"
            "\t unallocated bytes %u\n"
            "\t allocated bytes %u\n",
            get_unallocated_size(fs->heap),
            get_allocated_size(fs->heap)
            );
}
