#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>

typedef struct {
    void* object;
    void (*dtor)(void*);
    size_t count;
}shared_ptr;

shared_ptr* make_shared(void* object, void (*dtor)(void*)) {
    shared_ptr* ptr = (shared_ptr*)malloc(sizeof(shared_ptr));
    if(!ptr) {
        return NULL;
    }
    ptr->object = object;
    ptr->dtor = dtor;
    ptr->count = 1;
    return ptr;
}

void free_shared(shared_ptr* ptr) {
    if(!ptr) {
        return;
    }
    ptr->count--;
    if(!ptr->count) {
        ptr->dtor(ptr->object);
        free(ptr);
    }
}

shared_ptr* copy_shared(shared_ptr* ptr) {
    if(!ptr) {
        return NULL;
    }
    ptr->count++;
    return ptr;
}

void* deref_shared(shared_ptr* ptr) {
    if(!ptr) {
        return NULL;
    }
    return ptr->object;
}

void enter(size_t n, ...) {
    va_list list;
    va_start(list, n);
    for(size_t i=0; i < n; ++i) {
        shared_ptr* ptr = va_arg(list, shared_ptr*);
        copy_shared(ptr);
    }
    va_end(list);
}

void leave(size_t n, ...) {
    va_list list;
    va_start(list, n);
    for(size_t i=0; i < n; ++i) {
        shared_ptr* ptr = va_arg(list, shared_ptr*);
        free_shared(ptr);
    }
    va_end(list);
}
